import styles from "./Button.module.scss";
import PropTypes from "prop-types";

function Button({ background, onClick, text, modalId }) {
  return (
    <button
      className={styles.Button}
      style={{ background }}
      onClick={onClick}
      id={modalId}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  background: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
  modalId: PropTypes.string,
};

Button.defaultProps = {
  background: "wheat",
};

export default Button;
