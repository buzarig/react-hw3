import React, { Component } from "react";
import { Icon } from "@iconify/react";
import styles from "./Cart.module.scss";

class Cart extends Component {
  render() {
    return (
      <>
        <div className={styles.Container}>
          <span className={styles.Count}>{this.props.counter}</span>
          <Icon
            icon="material-symbols:shopping-cart-outline-rounded"
            width="50"
            height="50"
            color="white"
          />
        </div>
      </>
    );
  }
}

export default Cart;
