import React, { Component } from "react";
import styles from "./Favorite.module.scss";
import { Icon } from "@iconify/react";

class Favorite extends Component {
  render() {
    return (
      <>
        <div className={styles.Container}>
          <span className={styles.Count}>{this.props.counter}</span>
          <Icon icon="bi:star" width="50" height="50" color="white" />
        </div>
      </>
    );
  }
}

export default Favorite;
