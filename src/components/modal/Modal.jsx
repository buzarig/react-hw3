import React, { useEffect, useState } from "react";
import Button from "../button/Button";
import styles from "./Modal.module.scss";
import modalSettings from "./modalSettings";
import PropTypes from "prop-types";

function Modal(props) {
  const [settings, setSettings] = useState({});

  useEffect(() => {
    const modalDeclaration = modalSettings.find(
      (item) => item.modalId === props.modalId
    );
    setSettings(modalDeclaration.modalProps);
  }, [props.modalId]);

  const { title, description, background, closeButton, actions } = settings;

  return (
    <div
      className={styles.Wrapper}
      onClick={(evt) => evt.currentTarget === evt.target && props.close()}
    >
      <div style={{ background: background }} className={styles.Modal}>
        <div className={styles.Header}>
          {title}
          {closeButton && (
            <Button onClick={props.close} text="x" background="blue"></Button>
          )}
        </div>
        <div className={styles.Text}>{description}</div>
        <div className={styles.Actions}>
          {actions &&
            actions.map((action) => (
              <Button
                background={action.background}
                text={action.text}
                key={action.type}
                onClick={
                  action.type === "submit"
                    ? () => {
                        props.submit();
                        props.close();
                      }
                    : props.close
                }
              />
            ))}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  modalId: PropTypes.string,
  close: PropTypes.func,
  submit: PropTypes.func,
};

export default Modal;
