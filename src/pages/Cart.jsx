import React, { useState, useEffect } from "react";
import Products from "../components/products/Products";
import Modal from "../components/modal/Modal";

export function CartPage() {
  const [products, setProducts] = useState([]);
  const [favoriteProducts, setFavoriteProducts] = useState(
    JSON.parse(localStorage.getItem("Favorite")) ||
      localStorage.setItem("Favorite", JSON.stringify([]))
  );
  const [modal, setModal] = useState({
    showModal: false,
    modalId: null,
    modalSubmit: null,
  });
  const [cartProducts, setCartProducts] = useState(
    JSON.parse(localStorage.getItem("Cart")) ||
      localStorage.setItem("Cart", JSON.stringify([]))
  );

  useEffect(() => {
    const productsInCart = JSON.parse(localStorage.getItem("Cart"));
    setProducts(productsInCart);
  }, [cartProducts]);

  const updateCart = () => {
    setCartProducts(JSON.parse(localStorage.getItem("Cart")));
  };

  if (cartProducts.length === 0) {
    return <p>no products in cart.</p>;
  }

  const updateFavorite = () => {
    setFavoriteProducts(JSON.parse(localStorage.getItem("Favorite")));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    setModal({
      showModal: true,
      modalId,
      modalSubmit,
    });
  };

  const handleCloseModal = () => {
    setModal({
      showModal: false,
      modalId: null,
      modalSubmit: null,
    });
  };

  return (
    <>
      <Products
        cartProducts={cartProducts}
        products={products}
        openModal={handleOpenModal}
        favoriteProducts={favoriteProducts}
        updateFavorite={updateFavorite}
        updateCart={updateCart}
        cartRemover={true}
      />
      {modal.showModal && (
        <Modal
          close={handleCloseModal}
          modalId={modal.modalId}
          submit={modal.modalSubmit}
        />
      )}
    </>
  );
}
