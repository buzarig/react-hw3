import React, { useState, useEffect } from "react";
import Products from "../components/products/Products";
import Modal from "../components/modal/Modal";

export function FavoritePage() {
  const [products, setProducts] = useState([]);
  const [favoriteProducts, setFavoriteProducts] = useState(
    JSON.parse(localStorage.getItem("Favorite")) ||
      localStorage.setItem("Favorite", JSON.stringify([]))
  );
  const [modal, setModal] = useState({
    showModal: false,
    modalId: null,
    modalSubmit: null,
  });
  const [cartProducts, setCartProducts] = useState(
    JSON.parse(localStorage.getItem("Cart")) ||
      localStorage.setItem("Cart", JSON.stringify([]))
  );

  useEffect(() => {
    const productsInFavorite = JSON.parse(localStorage.getItem("Favorite"));
    setProducts(productsInFavorite);
  }, [favoriteProducts]);

  const updateFavorite = () => {
    setFavoriteProducts(JSON.parse(localStorage.getItem("Favorite")));
  };

  if (favoriteProducts.length === 0) {
    return <p>no products in favorite.</p>;
  }

  const updateCart = () => {
    setCartProducts(JSON.parse(localStorage.getItem("Cart")));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    setModal({
      showModal: true,
      modalId,
      modalSubmit,
    });
  };

  const handleCloseModal = () => {
    setModal({
      showModal: false,
      modalId: null,
      modalSubmit: null,
    });
  };

  return (
    <>
      <Products
        favoriteProducts={favoriteProducts}
        cartProducts={cartProducts}
        products={products}
        updateFavorite={updateFavorite}
        updateCart={updateCart}
        openModal={handleOpenModal}
        favoriteRemover={true}
      />
      {modal.showModal && (
        <Modal
          close={handleCloseModal}
          modalId={modal.modalId}
          submit={modal.modalSubmit}
        />
      )}
    </>
  );
}
