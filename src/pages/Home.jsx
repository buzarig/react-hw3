import Products from "../components/products/Products";
import React, { useState, useEffect } from "react";
import Modal from "../components/modal/Modal";
import Cart from "../components/cart/Cart";
import Favorite from "../components/favorite/Favorite";

export function Home() {
  const [products, setProducts] = useState([]);
  const [cartProducts, setCartProducts] = useState(
    JSON.parse(localStorage.getItem("Cart")) ||
      localStorage.setItem("Cart", JSON.stringify([]))
  );
  const [favoriteProducts, setFavoriteProducts] = useState(
    JSON.parse(localStorage.getItem("Favorite")) ||
      localStorage.setItem("Favorite", JSON.stringify([]))
  );
  const [modal, setModal] = useState({
    showModal: false,
    modalId: null,
    modalSubmit: null,
  });

  useEffect(() => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((products) => {
        setProducts(products);
      });
  }, []);

  const updateCart = () => {
    setCartProducts(JSON.parse(localStorage.getItem("Cart")));
  };

  const updateFavorite = () => {
    setFavoriteProducts(JSON.parse(localStorage.getItem("Favorite")));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    setModal({
      showModal: true,
      modalId,
      modalSubmit,
    });
  };

  const handleCloseModal = () => {
    setModal({
      showModal: false,
      modalId: null,
      modalSubmit: null,
    });
  };

  return (
    <>
      <header className="header">
        <Favorite counter={favoriteProducts.length} />
        <Cart counter={cartProducts.length} />
      </header>
      {console.log(cartProducts)}
      <Products
        products={products}
        favoriteProducts={favoriteProducts}
        cartProducts={cartProducts}
        openModal={handleOpenModal}
        updateFavorite={updateFavorite}
        updateCart={updateCart}
      />
      {modal.showModal && (
        <Modal
          close={handleCloseModal}
          modalId={modal.modalId}
          submit={modal.modalSubmit}
        />
      )}
    </>
  );
}
